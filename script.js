class application {
    constructor(canvas){
        this.objects=[];
        this.saved = true;
        this.canvas = canvas;
    }
    new() {
        console.debug("[new] exec");
        if(this.objects === undefined){
            console.debug("[new] objects is undefined. aborting");
            return -1;
        }
        if(this.saved === undefined){
            console.debug("[new] saved is undefined. aborting");
            return -1;
        }
        if(this.objects.length !== 0 || !this.saved){
            console.debug("[new] objects not empty or saved === false");
            console.debug("[new] asking confirm");
            if(confirm("You have unsaved work. Are you sure you want to make a new project?")){
                console.debug("[new] confirm true");
            }
            else{
                console.debug("[new] confirm false");
                console.debug("[new] aborting");
                return
            }
        }
        this.objects = [];
        this.saved = true;
        console.debug("[new] objects = [] and saved = true");
    };
}

let app = new application($('canvas')[0]);